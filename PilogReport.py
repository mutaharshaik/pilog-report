import argparse
import os
import pandas as pd
from collections import defaultdict
from datetime import datetime


def main(args):
    # validate input file
    if not os.path.isfile(args.input_OysterLinkIndex):
        raise ValueError(f"OysterLinkIndex file does not exist. No file located at: {args.input_OysterLinkIndex}")
    if not args.input_OysterLinkIndex.endswith('.tsv'):
        raise ValueError("Input file must be a .tsv file")

    if not os.path.isfile(args.input_DWMLinkIndex):
        raise ValueError(f"DWMLinkIndex file does not exist. No file located at: {args.input_DWMLinkIndex}")
    if not args.input_DWMLinkIndex.endswith('.csv'):
        raise ValueError("Input file must be a .csv file")

    # validate output file
    if not args.output.endswith('.xlsx'):
        raise ValueError("Output file must be a .xlsx file")

    # Cleaning the Oyster Output file(LinkIndex.tsv)
    df_Linkindex = pd.read_csv(args.input_OysterLinkIndex, sep='\t')
    df_Linkindex = df_Linkindex.drop('Rule', axis=1)
    df_Linkindex['RefID'] = df_Linkindex['RefID'].str.removeprefix('TWK.')

    ## Create a column - Size of CLuster with Frequency (no of times occured) of OysterID
    # it says how many records exist in a single oyster ID
    df_Linkindex['freq_size'] = df_Linkindex.groupby('OysterID')['OysterID'].transform('count')

    #Dictionary 1:
    # key : Rec ID
    # value: Oyster clusterID(from lenient Link Index)
    dict1 = pd.Series(df_Linkindex['OysterID'].values,index=df_Linkindex['RefID']).to_dict()

    #Dictionary 2:
    # key : Oyster cluster ID and
    # value : size of the cluster (like a cluster profile)
    dict2 = pd.Series(df_Linkindex['freq_size'].values,index=df_Linkindex['OysterID']).to_dict()

    # Cleaning the DWM Output file(DWMLinkindex.csv)
    df_dwm = pd.read_csv(args.input_DWMLinkIndex)
    df_dwm = df_dwm.drop('References',axis=1)

    #Dictionary 3:
    # Key :  DWM clusterID
    # Value : RecID
    dict3 = df_dwm.groupby('ClusterID')['RecID'].apply(list).to_dict()

    dict4=dict()
    IdenticalDWMClusters=list()
    IdenticalDWMClusters_distributionBy_size=dict()

    # lookup of OysterIDs
    for clusterID, RecIDs in dict3.items():
        oysterID_PerClusterID = set()
        print(f"ClusterID {clusterID} -> RecID-List {RecIDs}")
        for i in RecIDs:
            oysterID = dict1.get(i)
            print(f"RecID {i} -> dict2 Oyster {oysterID}")
            oysterID_PerClusterID.add(oysterID)             #for unique oyster ID
            dict4[clusterID] = oysterID_PerClusterID

    # validation of Size
    for key, OysterIdSet in dict4.items():     # key=clusterID  and value=OysterIdSet
        if (len(OysterIdSet)> 1):
            print(f"DWM and Oyster Cluster - Dont Match for DWM ClusterID : {key}")
        else:
            print(f"DWM and Oyster Cluster - Match for DWM ClusterID : {key}")
            oysterID = next(iter(OysterIdSet))
            sizeOfOysterCluster = dict2.get(oysterID)
            print(f"Size of OysterCluster : {sizeOfOysterCluster}")
            DWMCluster = dict3.get(key)               # key=clusterID
            sizeOfDWMCluster = len(DWMCluster)
            print(f"Size of DWMCluster : {sizeOfDWMCluster}")
            print("*"*40)
            if(sizeOfOysterCluster == sizeOfDWMCluster):
                print(f"Identical Clusters DWM ClusterID- {key}")
                IdenticalDWMClusters.append(key)
                IdenticalDWMClusters_distributionBy_size[key] = sizeOfOysterCluster    #IdenticalDWMClusters_distributionBy_size(key,value)= clusterID, sizeOfOysterCluster


    print(f"Number of Identical Clusters {len(IdenticalDWMClusters)}")
    print(f"Total Number of DWM Clusters - {len(dict3.keys())}")
    print(f"Identical DWMClusters Profile {IdenticalDWMClusters_distributionBy_size}")

    dict_groupedBySize = defaultdict(list)            #IdenticalDWMClusters=list()
    for key, val in sorted(IdenticalDWMClusters_distributionBy_size.items()):
        dict_groupedBySize[val].append(key)
    # printing Grouped dict
    print(f"Grouped dictionary is : {str(dict(dict_groupedBySize))}")

    # Identical Cluster Profile
    dict5= dict()
    for key, value in sorted(dict(dict_groupedBySize).items()):
        dict5[key] = len(value)
    print(f"Identical Cluster Profile {dict5}")
    df_IdenticalClusterProfile = pd.DataFrame(list(dict5.items()),
                                              columns=['IdenticalClusterProfile_size', 'IdenticalClusterProfile_NoOfClusters'])

    # DWM CLuster Profile
    df_dwm = df_dwm.drop('RecID', axis=1)
    df_dwm['freq_size'] = df_dwm.groupby('ClusterID')['ClusterID'].transform('count')
    series_DWMProfile = df_dwm.groupby('freq_size')['ClusterID'].nunique()
    df_DWMProfile = pd.DataFrame(series_DWMProfile.reset_index(name = "DWMProfile_noOfClusters"))
    df_DWMProfile = df_DWMProfile.rename(columns={'freq_size': 'DWMProfile_size'})

    # Oyster Cluster profile
    series_OysterProfile = df_Linkindex.groupby('freq_size')['OysterID'].nunique()
    df_OysterProfile = pd.DataFrame(series_OysterProfile.reset_index(name = "OysterProfile_noOfClusters"))
    df_OysterProfile = df_OysterProfile.rename(columns={'freq_size': 'OysterProfile_size'})

    ## Concat all Profiles and write to Output File
    df_report = pd.concat([df_DWMProfile, df_OysterProfile, df_IdenticalClusterProfile ],axis=1)

    date = datetime.now().strftime("%Y_%m_%d-%I:%M:%S_%p")
    df_report.to_excel(args.output, header=True, index=False, encoding='utf-8', na_rep='None')

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_OysterLinkIndex", help="input Oyster file", required=True)
    parser.add_argument("--input_DWMLinkIndex", help="input DWM file", required=True)
    parser.add_argument("--output", help="output xlsx IdenticalClusters file", required=True)
    args = parser.parse_args()
    main(args)
