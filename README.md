# README #
### What is this repository for? ###

* Quick summary: 
1. It profiles the Identical clusters in both the Oyster Cluster and Data Washing Machine Clusters.
2. It also gives the output in Excel with list of Oyster and DWM clusters along with Indentical Cluster Profiles.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

# How to run
1. pip install requirements.txt: `pip install -r requirements.txt`
2. run `python PilogReport.py` with arguments found in Command Line Arguments section below.

Example:
```
python PilogReport.py \
    --input_OysterLinkIndex=resources/Linkindex.tsv \
    --input_DWMLinkIndex=resources/DWMLinkindex.csv \
    --output=output/report.xlsx
```
## Command line arguments
1. `--input_OysterLinkIndex`: The input Oyster LinkIndex file . Must be a full path to a tsv file. For example, `--input_OysterLinkIndex=/Users/mohammedmutaharshaik/Documents/BitBucketPilog/pilog-report/resources/Linkindex.tsv`.
2. `--input_DWMLinkIndex`: The input DWM LinkIndex file. Must be a full path to a csv file. For example, `--input_DWMLinkIndex=/Users/mohammedmutaharshaik/Documents/BitBucketPilog/pilog-report/resources/DWMLinkindex.csv`.
3. `--output`: The output file. Must be a full path to a csv file. For example, `--output=/Users/mohammedmutaharshaik/Documents/BitBucketPilog/pilog-report/output/report.xlsx`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

######################################################################################

# Comparision Report Details


* Quick summary:
1. Analysis to see the Different comparisions between the Oyster Machine and the Data Washing Machine 
whether  DWM clusters are the Subsets of Oyster Clusters 
### How do I get set up? ###

# How to run
1. pip install requirements.txt: `pip install -r requirements.txt`
2. run `python ComparisionReport.py` with arguments found in Command Line Arguments section below.

Example:
```
python ComparisionReport.py \
    --input_Newlinkindx_joinby_oyester_truthID=resources/Newlinkindx_joinby_oyester_truthID.csv \
```
## Command line arguments
1. `--input_Newlinkindx_joinby_oyester_truthID`: The input DWM LinkIndex file. Must be a full path to a csv file. For example, `--input_Newlinkindx_joinby_oyester_truthID=/Users/mohammedmutaharshaik/Documents/BitBucketPilog/pilog-report/resources/Newlinkindx_joinby_oyester_truthID.csv`
