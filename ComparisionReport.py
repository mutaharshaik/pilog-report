import argparse as argparse
import pandas as pd
import logging
import os

def main(args):
    # validate input file
    if not os.path.isfile(args.input_Newlinkindx_joinby_oyester_truthID):
        raise ValueError(f"Newlinkindx_joinby_oyester_truthID file does not exist. No file located at: {args.input_Newlinkindx_joinby_oyester_truthID}")
    if not args.input_Newlinkindx_joinby_oyester_truthID.endswith('.csv'):
        raise ValueError("Input file must be a .csv file")

    # Cleaning the file
    input_file="/Users/mohammedmutaharshaik/Documents/BitBucketPilog/pilog-report/resources/Newlinkindx_joinby_oyester_truthID.csv"
    df_file = pd.read_csv(input_file)
    df_file = df_file.drop('LenientID',axis=1)

    #Dictionary 1: Dict_Oyster
    # Key :  Oyster_CID
    # Value : list of RefIDs
    Dict_Oyster = df_file.groupby('OysterID')['RecID'].apply(list).to_dict()

    #Dictionary 2: Dict_DWM
    # Key :  DWM cluster ID
    # Value : list of RefIDs
    Dict_DWM = df_file.groupby('ClusterID')['RecID'].apply(list).to_dict()

    #Dictionary 3: Dict_Oyster_DWM
    # Key :  RefID,
    # Value : pair (OysterID, DwmID)
    Dict_Oyster_DWM = df_file.groupby('RecID')[['OysterID','ClusterID']].apply(lambda g: list(map(tuple, g.values.tolist()))).to_dict()

    logging.getLogger().setLevel(logging.DEBUG)
    IdenticalCluster_IntersectionCount_1= {}
    IdenticalCluster_IntersectionCount_gt1={}
    UnIdenticalCluster_finalDwmSet_gtOysterSet={}
    UnIdenticalCluster_finalDwmSet_ltOysterSet={}

    #look up for list of RecID from Dict_Oyster
    for OysterID, RecIDList in Dict_Oyster.items():
        finalDwmSet=set()
        xOysterList=[]
        logging.info(f"OysterID {OysterID} -> ListOfRecID {RecIDList} -> size {len(RecIDList)}")
        # lookup for DWM_CID from Dict_Oyster_DWM
        for RecID in RecIDList:
            DWM_CID = Dict_Oyster_DWM.get(RecID)[0][1]
            logging.debug(f"RecID {RecID} --> Dict_Oyster_DWM:CID {DWM_CID}")
            #look up for List of RecIDs from Dict_DWM
            listRecs = Dict_DWM.get(DWM_CID)
            logging.debug(f"DWM_CID {DWM_CID} --> Dict_DWM_listRecs {listRecs}")
            xOysterList.extend(listRecs)
        # Converting lists of RecIDs to sets
        finalDwmSet = set(xOysterList)
        sizeOfFinalDwmSet = len(finalDwmSet)
        sizeOfOysterCluster = len(RecIDList)
        logging.info(f" Final OysterID {OysterID} - finalDwmSet {finalDwmSet} --> size {sizeOfFinalDwmSet}")
        logging.info(f" OysterID {OysterID} -- Size of Oysterset {sizeOfOysterCluster} , Size of finalDwmSet {sizeOfFinalDwmSet}")
        if (sizeOfFinalDwmSet > sizeOfOysterCluster):
            logging.info(f"The Oyster Cluster splits some of the DWM clusters")
            print(f"OysterID {OysterID} --> Size of OysterSet {sizeOfOysterCluster} : DWM_CID {DWM_CID} Size of finalDwmSet {sizeOfFinalDwmSet}")
            UnIdenticalCluster_finalDwmSet_gtOysterSet[OysterID] = sizeOfOysterCluster
        elif(sizeOfFinalDwmSet == sizeOfOysterCluster):
            if(sizeOfOysterCluster==1):
                logging.info(f" The two clusters are identical (Intersection count is 1)")
                IdenticalCluster_IntersectionCount_1[OysterID] = sizeOfOysterCluster
            else:
                logging.info(f" DWM clusters are identical and refinement of the Oyster Cluster (Intersection count is > 1)")
                IdenticalCluster_IntersectionCount_gt1[OysterID] = sizeOfOysterCluster
    else:
            logging.info(f"The DWM clusters splits some of the Oyster Cluster")
            UnIdenticalCluster_finalDwmSet_ltOysterSet[OysterID]=sizeOfOysterCluster
    logging.info("*"*60)
    logging.info(f"Size of UnIdenticalCluster_finalDwmSet_gtOysterSet:  {len(UnIdenticalCluster_finalDwmSet_gtOysterSet)}")
    logging.info(f"Size of UnIdenticalCluster_finalDwmSet_ltOysterSet:  {len(UnIdenticalCluster_finalDwmSet_ltOysterSet)}")
    logging.info(f"Size of IdenticalCluster_IntersectionCount_1:  {len(IdenticalCluster_IntersectionCount_1)}")
    logging.info(f"Size of IdenticalCluster_IntersectionCount_gt1:  {len(IdenticalCluster_IntersectionCount_gt1)}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_Newlinkindx_joinby_oyester_truthID", help="input Newlinkindx_joinby_oyester_truthID file", required=True)
    args = parser.parse_args()
    main(args)